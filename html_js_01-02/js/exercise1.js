// var slideIndex = 1;
// showSlides(slideIndex);

// function plusSlides(n) {
//   showSlides(slideIndex += n);
// }

// function currentSlide(n) {
//   showSlides(slideIndex = n);
// }

// function showSlides(n) {
//   var i;
//   var slides = document.getElementsByClassName("mySlides");
//   var dots = document.getElementsByClassName("dot");
//   if (n > slides.length) {
//     slideIndex = 1
//   }
//   if (n < 1) {
//     slideIndex = slides.length
//   }
//   for (i = 0; i < slides.length; i++) {
//     slides[i].style.display = "none";
//   }
//   for (i = 0; i < dots.length; i++) {
//     dots[i].className = dots[i].className.replace(" active", "");
//   }
//   slides[slideIndex - 1].style.display = "block";
//   //   dots[slideIndex-1].className += " active";

// }
// // setInterval(function(){plusSlides(1)},2000);



// //  jQuery

// $("body").bind("dragover", function(e){
//     var dragX = e.pageX, dragY = e.pageY;

//     console.log("X: "+dragX+" Y: "+dragY);
// });

var Slider = function (params) {
  this.el = params.el;
  this.autoplay = params.autoplay;
  this.element = null;
  this.listItem = null;
  this.nextEl = params.nextEl;
  this.prevEl = params.prevEl;
  this.idWrapper = params.idWrapper;

  this.init = function () {
    this.element = document.getElementById(this.el);
    this.listItem = document.getElementsByClassName(this.el);
    this.show(0);
    var self = this;
    document.getElementById(this.nextEl).addEventListener('click', function (e) {
      self.next();
    }, false);
    document.getElementById(this.prevEl).addEventListener('click', function (e) {
      self.prev();
    }, false);

    if (this.autoplay) {
      setInterval(function () {
        self.next()
      }, self.autoplay);
    }
    document.getElementById(this.idWrapper).addEventListener("dragstart", function (event) {
      // this.className = "active1";
      // store a ref. on the dragged elem
      event = event || window.event;
      var start = event.pageX;
      a = start;
      // console.log("X: " + a);

    }, false);
    document.getElementById(this.idWrapper).addEventListener("dragend", function (event) {
      event = event || window.event;
      var end = event.pageX
      b = end;
      if (a < b) {
        self.prev();
      } else {
        self.next();
      }
    }, false);

  }
  this.init();
}


Slider.prototype.next = function () {
  let current = this.current();
  let nextIndex = current + 1;
  if (current == this.listItem.length - 1) nextIndex = 0;
  this.show(nextIndex);
}
Slider.prototype.prev = function () {
  let current = this.current();
  let nextIndex = current - 1;
  if (current == 0) nextIndex = this.listItem.length - 1;
  this.show(nextIndex);
}
Slider.prototype.show = function (index) {
  for (let i = 0; i < this.listItem.length; i++) {
    if (index == i) {
      this.listItem[i].className = this.el + ' active';
    } else {
      this.listItem[i].className = this.el + ' hidden';
    }
  }
}
Slider.prototype.current = function () {
  for (let i = 0; i < this.listItem.length; i++) {
    let className = this.listItem[i].className;
    if (className.split(' ').indexOf('active') > 0) {
      return i;
    }
  }
}

var slider = new Slider({
  el: 'mySlides',
  nextEl: 'icon-prev',
  prevEl: 'icon-next',
  idWrapper: 'banner',
  autoplay: 4000
});

// var slider = new Slider({
//   el: 'mySlides1',
//   // autoplay: 1000
// });
// var slider = new Slider({
//   el: 'mySlides',
//   // autoplay: 5000
// });